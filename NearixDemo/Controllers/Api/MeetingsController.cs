﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using NearixDemo.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NearixDemo.Controllers.Api
{
	[Authorize]
    public class MeetingsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Meetings/Mine
        [Route("api/Meetings/Mine")]
        public IQueryable<Meeting> GetMeetingsMine()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var um = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            
            var CurrentUser = um.FindById(User.Identity.GetUserId());
            
            return db.Meetings.Where(m => m.User.Id == CurrentUser.Id).OrderBy(m => m.At).Include("Client").Include("User");
        }

        // GET: api/Meetings
        public IQueryable<Meeting> GetMeetings()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Meetings.OrderBy(m => m.At).Include("Client").Include("User");
        }

        // GET: api/Meetings/Count
        [Route("api/Meetings/Count")]
        public int GetMeetingsCount()
        {
            return db.Meetings.Count();
        }

        // GET: api/Meetings/5
        [ResponseType(typeof(Meeting))]
        public IHttpActionResult GetMeeting(int id)
        {

            Meeting meeting = db.Meetings.Find(id);
            if (meeting == null)
            {
                return NotFound();
            }

            return Ok(meeting);
        }

        // PUT: api/Meetings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMeeting(int id, Meeting meeting)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != meeting.Id)
            {
                return BadRequest();
            }

            db.Entry(meeting).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeetingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Meetings
        [ResponseType(typeof(Meeting))]
        public IHttpActionResult PostMeeting(Meeting meeting)
        {
            var um = new ApplicationUserManager(new UserStore<ApplicationUser>(db));

            var CurrentUser = um.FindById(User.Identity.GetUserId());
            meeting.User = CurrentUser;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Meetings.Add(meeting);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = meeting.Id }, meeting);
        }

        // DELETE: api/Meetings/5
        [ResponseType(typeof(Meeting))]
        public IHttpActionResult DeleteMeeting(int id)
        {
            Meeting meeting = db.Meetings.Find(id);
            if (meeting == null)
            {
                return NotFound();
            }

            db.Meetings.Remove(meeting);
            db.SaveChanges();

            return Ok(meeting);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeetingExists(int id)
        {
            return db.Meetings.Count(e => e.Id == id) > 0;
        }
    }
}