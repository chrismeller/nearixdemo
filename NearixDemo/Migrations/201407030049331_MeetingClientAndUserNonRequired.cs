namespace NearixDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MeetingClientAndUserNonRequired : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Meetings", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Meetings", new[] { "Client_Id" });
            DropIndex("dbo.Meetings", new[] { "User_Id" });
            AlterColumn("dbo.Meetings", "Client_Id", c => c.Int());
            AlterColumn("dbo.Meetings", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Meetings", "Client_Id");
            CreateIndex("dbo.Meetings", "User_Id");
            AddForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients", "Id");
            AddForeignKey("dbo.Meetings", "User_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Meetings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients");
            DropIndex("dbo.Meetings", new[] { "User_Id" });
            DropIndex("dbo.Meetings", new[] { "Client_Id" });
            AlterColumn("dbo.Meetings", "User_Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Meetings", "Client_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Meetings", "User_Id");
            CreateIndex("dbo.Meetings", "Client_Id");
            AddForeignKey("dbo.Meetings", "User_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients", "Id", cascadeDelete: true);
        }
    }
}
