namespace NearixDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixMeetingFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients");
            DropIndex("dbo.Meetings", new[] { "Client_Id" });
            RenameColumn(table: "dbo.Meetings", name: "Client_Id", newName: "ClientId");
            AlterColumn("dbo.Meetings", "ClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Meetings", "ClientId");
            AddForeignKey("dbo.Meetings", "ClientId", "dbo.Clients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Meetings", "ClientId", "dbo.Clients");
            DropIndex("dbo.Meetings", new[] { "ClientId" });
            AlterColumn("dbo.Meetings", "ClientId", c => c.Int());
            RenameColumn(table: "dbo.Meetings", name: "ClientId", newName: "Client_Id");
            CreateIndex("dbo.Meetings", "Client_Id");
            AddForeignKey("dbo.Meetings", "Client_Id", "dbo.Clients", "Id");
        }
    }
}
