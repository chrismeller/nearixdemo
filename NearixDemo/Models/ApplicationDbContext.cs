﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using NearixDemo.Models;

namespace NearixDemo.Models
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false)
		{
		}

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}


		public DbSet<Client> Clients { get; set; }
		public DbSet<Meeting> Meetings { get; set; }
	}
}