﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NearixDemo.Models
{
    public class Meeting
    {
        public int Id { get; set; }

        [Required]
        public string Reason { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Scheduled At")]
        public DateTime At { get; set; }

        [ForeignKey("Client")]
        [Required]
        public int ClientId { get; set; }

        public virtual Client Client { get; set; }
		public virtual ApplicationUser User { get; set; }
    }
}