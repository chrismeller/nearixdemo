﻿angular.module('nearix', ['ngRoute', 'datePicker', 'mm.foundation'])
    .config(['$routeProvider', function ($routeProvider) {
    	'use strict';

    	$routeProvider
			.when('/', {
				controller: 'DashboardController',
				templateUrl: '/Scripts/app/partials/dashboard.html'
			})
    		.when('/clients', {
    			controller: 'ClientsController',
    			templateUrl: '/Scripts/app/partials/clients/list.html'
    		})
            .when('/clients/:clientId', {
                controller: 'ClientsEditController',
                templateUrl: '/Scripts/app/partials/clients/edit.html'
            })
            .when('/meetings', {
                controller: 'MeetingsController',
                templateUrl: '/Scripts/app/partials/meetings/list.html'
            })
            .when('/meetings/:meetingId', {
                controller: 'MeetingsEditController',
                templateUrl: '/Scripts/app/partials/meetings/edit.html'
            })
			.otherwise({ redirectTo: '/' });

    }]);