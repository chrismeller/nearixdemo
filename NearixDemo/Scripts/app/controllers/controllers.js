﻿'use strict';

var nearix = angular.module('nearix');

nearix.controller('DashboardController', ['$scope', '$http', function DashboardController($scope, $http) {
    'use strict';
    
    $http.get('/api/Meetings/Mine').success(function (data) {
        if (data.length) {
            $scope.nextMeeting = data[0].At;
        }
        else {
            $scope.nextMeeting = null;
        }
    });

    $scope.stats = {
        Clients: 0,
        Meetings: 0
    };
    $http.get('/api/Clients/Count').success(function (data) {
        $scope.stats.Clients = data;
    });

    $http.get('/api/Meetings/Count').success(function (data) {
        $scope.stats.Meetings = data;
    });
}]);

nearix.controller('ClientsController', ['$scope', '$http', function ClientsController($scope, $http) {
    'use strict';

    $http.get('/api/Clients').success(function (data) {
        $scope.clients = data;
    });

    $scope.deleteClient = function (client) {
        // @todo obviously, we should prompt for confirmation
        $http.delete('/api/Clients/' + client.Id).success(function () {
            $scope.clients.splice($.inArray(client, $scope.clients), 1);
        });
    };

    // our default sort
    $scope.sortedBy = {
        column: 'Name',
        reverse: false
    };
    $scope.sort = function (col) {
        // if we're already sorted by that column, reverse direction
        if ($scope.sortedBy.column == col) {
            $scope.sortedBy.reverse = !$scope.sortedBy.reverse;
        }
        else {
            // otherwise, sort by that column and reset to ascending
            $scope.sortedBy = {
                column: col,
                reverse: false
            };
        }
    };
}]);

nearix.controller('ClientsEditController', ['$scope', '$routeParams', '$http', '$location', function ClientsEditController($scope, $routeParams, $http, $location) {
    'use strict';

    if ($routeParams.clientId == 'add') {
        $scope.Title = 'Add Client';
    }
    else {
        $scope.Title = 'Edit Client';
        $http.get('/api/Clients/' + $routeParams.clientId).success(function (data) {
            $scope.client = data;
        });
    }
    
    $scope.saveClient = function (client) {
        if (client.Id) {
            $http.put('/api/Clients/' + client.Id, client).success(function () {
                $location.path('/clients');
            });
        }
        else {
            $http.post('/api/Clients', client).success(function () {
                $location.path('/clients');
            });
        }
        
    }
    
}]);

nearix.controller('MeetingsController', ['$scope', '$http', function MeetingsController($scope, $http) {
    'use strict';
    
    $http.get('/api/Meetings').success(function (data) {
        $scope.meetings = data;
    });

    $scope.deleteMeeting = function (meeting) {
        // @todo obviously, we should prompt for confirmation
        $http.delete('/api/Meetings/' + meeting.Id).success(function () {
            $scope.meetings.splice($.inArray(meeting, $scope.meetings), 1);
        });
    };

    // our default sort
    $scope.sortedBy = {
        column: 'At',
        reverse: false
    };
    $scope.sort = function (col) {
        // if we're already sorted by that column, reverse direction
        if ($scope.sortedBy.column == col) {
            $scope.sortedBy.reverse = !$scope.sortedBy.reverse;
        }
        else {
            // otherwise, sort by that column and reset to ascending
            $scope.sortedBy = {
                column: col,
                reverse: false
            };
        }
    };
}]);

nearix.controller('MeetingsEditController', ['$scope', '$routeParams', '$http', '$location', function MeetingsEditController($scope, $routeParams, $http, $location) {
    'use strict';

    if ($routeParams.meetingId == 'add') {
        $scope.Title = 'Add Meeting';
    }
    else {
        $scope.Title = 'Edit Meeting';
        $http.get('/api/Meetings/' + $routeParams.meetingId).success(function (data) {
            $scope.meeting = data;
        });
    }

    // the "reasons" you are scheduling a meeting
    $scope.reasons = [
        'An outbound phone call',
        'A call from the client (inbound phone call)',
        'Inbound email (from the client)',
        'Outbound email (from the salesperson)',
        'A marketing package was sent to the client',
        'The client viewed the marketing presentation'
    ];

    // get a list of all our clients to fill the select
    $http.get('/api/Clients').success(function (data) {
        $scope.clients = data;
    });

    $scope.saveMeeting = function (meeting) {
        if (meeting.Id) {
            meeting.Client = null;
            $http.put('/api/Meetings/' + meeting.Id, meeting).success(function () {
                $location.path('/meetings');
            });
        }
        else {
            $http.post('/api/Meetings', meeting).success(function () {
                $location.path('/meetings');
            });
        }

    }

}]);