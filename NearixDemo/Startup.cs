﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NearixDemo.Startup))]
namespace NearixDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
